package minieduca.plantillafuncionamiento;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetPlantillaFuncionamientoGreetingController {
    @GetMapping("/plantilla-greeting")
    public String index() {
        return "Hola desde Plantilla Funcionamiento :)";
    }
}