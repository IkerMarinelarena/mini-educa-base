package minieduca.educa;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetEducaGreetingController {
    @GetMapping("/educa-greeting")
    public String index() {
        return "Hola desde Educa :)";
    }
}