# Mini Educa

Ejercicio para trabajar en un proyecto real aplicando buenas prácticas

# Descripción

https://gitlab.com/IkerMarinelarena/educa-mini/-/milestones/1#tab-issues


## Propiedades

Java version: 18

Gradle version: 7.5

Uses Spring boot as base framework and GSON for object serialization. 

Build project:
- ./gradlew build

  :warning: :warning: **Build will fail because of tests, this is done in purpose, have a look at them!**  

Run project:
- ./gradlew bootRun

---

- Get Java version:
  - java --version

- Update Java version:
  - Download the JDK (version 17 right now)
  - Remember to adjust Intellij settings File > Project Structure > Project

---

- Get current Gradle version:
    - ./gradlew --version
    
- Update Gradle version ([take in account project's Java version for compatibility](https://docs.gradle.org/current/userguide/compatibility.html)):
    - ./gradlew wrapper --gradle-version <gradle-version>